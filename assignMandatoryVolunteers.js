const Volunteer = require('./classes.js').Volunteer;
const Slot = require('./classes.js').Slot;
const Assignment = require('./classes.js').Assignment;
const AssignmentToPost = require('./classes.js').AssignmentToPost;

const credentials = require('./config.js');
const qs = require("querystring");
var Logger = require('js-logger');
const fs = require('fs')

const axios = require('axios');
var token = "";
var instanceAPI = "";

const userId = "61541bda5151840013ee1b04";
const instanceAuth = axios.create({
    baseURL: 'https://preprod.overbookd.24heures.org/api',
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
});
const data = qs.stringify({
    // keycloak accepts www-urlencoded-form and not users
    username: credentials.username,
    password: credentials.password,
});

function removeItemOnce(arr, value) {
    var index = arr.indexOf(value);
    if (index > -1) {
        arr.splice(index, 1);
    }
    return arr;
}

function getToken(){
    return instanceAuth.post('/login', data)
        .then(function (response) {
            token = "Bearer " + response.data.token;
        })
        .catch(function (error) {
            console.log(error);
        });
}

async function getUser() {
    try {
        const data = await getToken();
        instanceAPI = axios.create({
            baseURL: 'https://preprod.overbookd.24heures.org/api',
            timeout: 2000,
            headers:{'Authorization': token}
        });
        const response = await instanceAPI.get('/user');
        return response.data;
    } catch (error) {
        console.error(error);
    }
}

async function setUsers(localUserDb = null){
    try{
        let volunteers = [];
        console.log(localUserDb==null);
        let users = null;
        if(localUserDb != null) { //In case of local DB use
            users = localUserDb;
            console.log("userDbNotNull");
            for (let i = 0; i < users.length; i++) {
                volunteers = users.map(u =>
                    new Volunteer(u.firstname, u.lastname, u.nickname, u.team, u.friends, u._id,
                        u.isCouple, u.sex, u.email, u.phone, u.handicap, u.hasDriverLicense, u.driverLicenseDate, u.birthdate,
                        u.departement, u.year, u.availabilities));
                return volunteers;
            }
        }
        else {
            users = await getUser();
            timeslots = await getTimeslots();

            console.log(timeslots);

            let timeslotsAvailabilitiesTab = [];
            for (let i = 0; i < users.length; i++) {
                let timeslotsAvailabilities = [];
                for (let j = 0; j < users[i].availabilities.length; j++) {
                    console.log(users[i].availabilities[j]);
                    let timeslot = timeslots.find(t => t._id === users[i].availabilities[j]);
                    timeslotsAvailabilities.push(timeslot);
                }
                timeslotsAvailabilitiesTab.push(timeslotsAvailabilities);
            }
            console.log(timeslotsAvailabilitiesTab);

            volunteers = users.map(u => new Volunteer(u.firstname, u.lastname, u.nickname, u.team, u.friends, u._id,
                u.isCouple, u.sex, u.email, u.phone, u.handicap, u.hasDriverLicense, u.driverLicenseDate, u.birthdate,
                u.departement, u.year));

            for (let i = 0; i < users.length; i++) {
                //Replace availabilities by timeslots availabilities tab
                volunteers[i].availabilities = timeslotsAvailabilitiesTab[i];
            }

            return volunteers;
        }
    } catch (error){
        console.log(error);
    }
};

async function getFT(){
    //const users = await setUsers();
    let data = await getToken();
    instanceAPI = axios.create({
        baseURL: 'https://preprod.overbookd.24heures.org/api',
        timeout: 2000,
        headers:{'Authorization': token}
    });
    const response = await instanceAPI.get('/FT');
    return response.data.data;
}

async function getTimeslots(){
    let data = await getToken();
    instanceAPI = axios.create({
        baseURL: 'https://preprod.overbookd.24heures.org/api',
        timeout: 2000,
        headers:{'Authorization': token}
    });
    const response = await instanceAPI.get('/timeslot');
    return response.data;
}

async function postAssignment(assignment){
    instanceAPI = axios.create({
        baseURL: 'https://preprod.overbookd.24heures.org/api',
        timeout: 2000,
        headers:{
            'Authorization': token,
            'Content-Type': 'application/json'
        }
    });
    try {
        console.log(assignment);
        const response = await instanceAPI.post('/assignment', assignment);
        return response.data;
    }
    catch (error) {
        console.log(error);
    }
}

async function setSlots(){
    try{
        const FT = await getFT();
        let slots = [];
        for (let i = 0; i<FT.length; i++){
            try{
                for (let j=0; j<FT[i].timeframes.length; j++) {
                    if (FT[i].timeframes[j].required !== undefined) {
                        for (let k = 0; k < FT[i].timeframes[j].required.length; k++) {
                            for (let l = 0; l < FT[i].timeframes[j].required[k].amount; l++) {
                                let newSlot = new Slot(FT[i]._id, FT[i].general.name, FT[i].instructions, FT[i].general.inCharge, FT[i].general.location, FT[i].status,
                                    "", FT[i].timeframes[j].start, FT[i].timeframes[j].end, FT[i].timeframes[j].required[k].type, FT[i].timeframes[j].required[k].team, FT[i].timeframes[j].required[k].user, FT[i].general.driverLicenseRequired);
                                slots.push(newSlot);
                            }
                        }
                    }
                }
            } catch (e) {
                console.log("Cette FT ne contient pas de créneau" + FT[i].general.name);
                console.log(e);
            }

        }
        return slots;
    } catch (error){
        console.log(error);
    }
}

function assignSlotToVolunteer(id, volunteer, slot){
    let assignment = new Assignment(id, volunteer, slot);
    var dStart = new Date(0);
    var dEnd = new Date(0);
    dStart.setUTCMilliseconds(slot.start);
    dEnd.setUTCMilliseconds(slot.end);
    console.debug("New Assignment : \n User : " + volunteer.firstname+ " " + volunteer.lastname +"\n Slot : " + slot.name +"\n Time : "+ dStart + " to " + dEnd + "\n Required type and team : " + slot.requiredType + " " + slot.requiredTeam);
    return assignment;
}
function getObjectById(tab, id){
    for (let i = 0; i < tab.length; i++){
        if (tab[i].id === id){
            return tab[i];
        }
    }
}

async function assignMandatoryVolunteers(slots, volunteers) {
    newAssignments = [];
    let slots_unassigned = slots;
    let volunteers_updated = volunteers;
    slots.filter(s => s.requiredType == "user").forEach(slot => {
        try{
            console.log("Volunteer id : " + volunteers[0].id);
            volunteer = getObjectById(volunteers, slot.requiredUser._id);
            console.log(slot.requiredUser.username);
        }
        catch (e) {
            console.log(e);
        }
        availability = isAvailable(volunteer, slot);
        if (availability != null) {
            newAssignments.push(assignSlotToVolunteer(availability._id, volunteer, slot));
            removeItemOnce(slots_unassigned, slot); //Remove slot from slots to assign
            for (let i = 0; i < volunteers_updated.length; i++) {
                if(volunteers[i] === volunteer){
                    removeItemOnce(volunteers_updated[i].availabilities, availability);
                }
            }
        } else {
            console.log("! Mandatory Assignment not assigned ! \n ");
        }
    });
    return {
        newAssignments,
        slots_unassigned,
        volunteers_updated
    };
}
async function assignVolunteersForStart(slots, volunteers, random=true){
    newAssignments = [];
    let slots_unassigned = slots;
    let volunteers_updated = volunteers;
    //console.log(slots);
    if(random === false) {
        slots.filter(s => s.requiredType === "team").forEach(slot => {
            let assigned = false;
            var dStart = new Date(0);
            dStart.setUTCMilliseconds(slot.start);
            console.log("NAME : " + slot.name + " " + dStart);
            console.log("requiredTeam : " + slot.requiredTeam);
            volunteers.forEach(volunteer => {
                if (assigned === false) {
                    availability = isAvailable(volunteer, slot);
                    if (availability != null) {
                        newAssignments.push(assignSlotToVolunteer("id", volunteer, slot));
                        removeItemOnce(slots_unassigned, slot); //Remove slot from slots to assign
                        removeItemOnce(volunteer.availabilities, availability); //Remove availability from user availabilities
                        assigned = true;
                        //console.log(volunteers[k].availabilities);
                    }
                }
            })
        });
    } else {
        console.log("SLOTS LENGTH NEW : " + slots.length)
        console.log("RANDOM MODE");
        count = 0;
        slots.forEach(slot => {
            count+=1;
            let assigned = false;
            var dStart = new Date(0);
            dStart.setUTCMilliseconds(slot.start);
            console.log("NAME : " + slot.name + " " + dStart);
            console.log("requiredTeam : " + slot.requiredTeam);
            newAssignments.push(assignSlotToVolunteer("id",volunteers[Math.floor(Math.random()*volunteers.length)], slot));
            removeItemOnce(slots_unassigned, slot); //Remove slot from slots to assign
        });
        console.log("COUNT " + count);
        console.log("YEAY" + newAssignments.length);
    }
    return {
        newAssignments,
        slots_unassigned,
        volunteers
    };
}

function isAvailable(volunteer, slot){
    try{
        console.log("test");
        for (let i = 0; i < volunteer.availabilities.length; i++) {
            //console.log(new Date(volunteer.availabilities[i].timeFrame.start) + "\n" + new Date(slot.start) + "\n" + new Date(volunteer.availabilities[i].timeFrame.end) + "\n" + new Date(slot.end) + "\n");
            if((new Date(volunteer.availabilities[i].timeFrame.start) <= new Date(slot.start)) && (new Date(volunteer.availabilities[i].timeFrame.end) >= new Date(slot.end))){
                console.log("availability found");
                return volunteer.availabilities[i]; //Normally there is only one slot which is possible so we can return directly
                //We return it to pop it from user availabilities after it is assigned
            }
            console.debug(volunteer.firstname + " " + volunteer.lastname + " Has no availability");
        }
    } catch (e) {
        //console.log(volunteer.firstname + " " + volunteer.lastname + " Has no availability");
    }
}

async function createAssignmentToPostFromAssignment(assignments){
    let assignmentsToPost = [];
    assignments.forEach(assignment => {
        assignmentsToPost.push(new AssignmentToPost("AutoAssignmentMandatory", assignment.volunteer.id, userId, assignment.slot.FTid, assignment.timeslotId));
    });
    return assignmentsToPost;
}

async function postAllAssignments(assignmentsToPost){
    let postAssignments = [];
    assignmentsToPost.forEach(assignment => {
        postAssignments.push(postAssignment(assignment));
    });
    return postAssignments;
}
//User local DB (volunteers.json)
//require('./volunteers.json')
let volunteers = setUsers().then(volunteers => {
    //console.log(volunteers);
    return volunteers;

}).then(volunteers => setSlots().then(slots => {
    //console.log(slots);
    return {slots, volunteers};
})).then(({ slots, volunteers } )=> {
    assAll = assignMandatoryVolunteers(slots, volunteers);
    //console.log(createAssignmentToPostFromAssignment(assAll.newAssignments));
    return assAll;
}).then(assAll => createAssignmentToPostFromAssignment(assAll.newAssignments)).then(assignmentsToPost => {
    //stringify the first assignment to post
    //console.log(JSON.stringify(assignmentsToPost[0]));
    postAssignment(JSON.stringify(assignmentsToPost[0]));
});
