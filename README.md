# auto-Affect

autoAffect de @HugoCo

Critères à prendre en compte pour l'AutoAffect:

1) respecter les dispo des bénévoles et le fait qu'un bénévole ne peut pas faire 2 taches en même temps (conflit)

2) respecter les orga requis et les roles minimum des orga demandés sur les créneaux (un bénévole ne peut pas faire la tache d'un orga par contre l'inverse est possible)

3) satisfaire un nombre d'heure de sommeil minimum paramètrable en fonction du role (par exemple 6h d'affilés entre 02h et 12h pour les bénévoles)

4) satisfaire des pauses minimales paramètrable de X temps tous les Y temps (par exemple: un bénévole doit avoir au minimum une pause d'une heure toutes les 4h)

5) prendre en compte des périodes de temps bloqués défini par les humains où l'on ne peut pas affecter le bénévole choisi (Je veux empècher manuellement que le bénévole ai un créneau à ce moment là meme si il est dispo)

6) mettre les amis ou les gens d'une meme asso ensemble pour leurs créneaux. Surtout pour les PS, gardiennages et point de passage, il faut absoluement que les 2 soient amis dans la mesure du possible (il faut que les 2 amis soient dispo en même temps)

7) ne pas affecter les bénévoles qui ne peuvent pas porter des choses lourdes sur du barrièrage ou du transport de matos

8) prendre en compte les préférences de tache demandés par les bénévoles (catering, animation ect)

9) Varier les types de taches pour chaque bénévole. tout le monde doit faire un peu de PS, de Bar ect (pas plus de X créneaux de PS, Gardiennage et point de passage, un peu de bar, un peu d'autre chose ect)

10) équilibrer le nombre de créneaux entre tous les bénévoles et par rapport à leur investissement (charisme). --> Affecter en premier les bénévoles avec le moins de dispo pour toutes les compléter puis remonter dans la liste.



# Requis
- node
- node-libcurl





