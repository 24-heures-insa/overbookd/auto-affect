package pkg

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/joho/godotenv"
)

type Request struct {
	Headers map[string]string
	Method  string
	Payload *strings.Reader
	Url     string
}

type ApiToken struct {
	Token string `json:"token"`
}

type DataStruct struct {
	Data []FT `json:"data"`
}
type Volunteer struct {
	ID                   string    `json:"_id"`
	Firstname            string    `json:"firstname"`
	Lastname             string    `json:"lastname"`
	Nickname             *string   `json:"nickname,omitempty"`
	Email                string    `json:"email"`
	Balance              *float64  `json:"balance,omitempty"`
	Phone                int64     `json:"phone"`
	Team                 []*string `json:"team"`
	Friends              []Friend  `json:"friends"`
	Availabilities       []string  `json:"availabilities"`
	HasDriverLicense     bool      `json:"hasDriverLicense"`
	HasPayedContribution bool      `json:"hasPayedContribution"`
	Pp                   *string   `json:"pp,omitempty"`
	Charisma             *int64    `json:"charisma"`
}

type Friend struct {
	Username string `json:"username"`
	ID       string `json:"id"`
}

type FT struct {
	ID         string        `json:"_id"`
	Fa         *int64        `json:"FA,omitempty"`
	General    string        `json:"general"`
	Status     string        `json:"status"`
	Validated  []string      `json:"validated"`
	Refused    []string      `json:"refused"`
	Equipments []interface{} `json:"equipments"`
	Timeframes []Timeframe   `json:"timeframes"`
	Count      int64         `json:"count"`
	V          int64         `json:"__v"`
	Comments   []Comment     `json:"comments"`
	Details    *Details      `json:"details,omitempty"`
	IsValid    *bool         `json:"isValid,omitempty"`
}

type Comment struct {
	Topic     string  `json:"topic"`
	Text      *string `json:"text,omitempty"`
	Time      string  `json:"time"`
	Validator string  `json:"validator"`
}

type Details struct {
	Locations   []string `json:"locations"`
	Description *string  `json:"description,omitempty"`
	IsValid     *bool    `json:"isValid,omitempty"`
}

type General struct {
	Name                  string    `json:"name"`
	InCharge              *InCharge `json:"inCharge,omitempty"`
	IsValid               *bool     `json:"isValid,omitempty"`
	DriverLicenseRequired *string   `json:"driverLicenseRequired,omitempty"`
	Location              *string   `json:"location,omitempty"`
}

type InCharge struct {
	ID       string `json:"_id"`
	Username string `json:"username"`
}

type Timeframe struct {
	Name     string     `json:"name"`
	Start    int64      `json:"start"`
	End      int64      `json:"end"`
	Timed    bool       `json:"timed"`
	Required []Required `json:"required"`
}

type Required struct {
	Type   string    `json:"type"`
	Amount int64     `json:"amount"`
	User   *InCharge `json:"user,omitempty"`
	Team   *string   `json:"team,omitempty"`
}

const url_base = "https://preprod.overbookd.24heures.org/api"

func call(reqToDo Request) (string, error) {

	client := &http.Client{}
	req, err := http.NewRequest(reqToDo.Method, reqToDo.Url, reqToDo.Payload)

	if err != nil {
		return "", err
	}
	for key, value := range reqToDo.Headers {
		req.Header.Add(key, value)
	}

	res, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", err
	}

	return string(body), nil
}

func GetToken() (string, error) {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatalf("Error loading .env file")
	}

	var username, password string
	username = os.Getenv("USERNAME")
	password = os.Getenv("PASSWORD")

	headers := make(map[string]string)
	headers["Content-Type"] = "application/x-www-form-urlencoded"
	content, err := call(Request{
		headers,
		"POST",
		strings.NewReader("username=" + username + "&password=" + password),
		url_base + "/login",
	})
	if err != nil {
		fmt.Println(err)
	}
	var jsonBlob = []byte(string(content))
	var extractedToken ApiToken
	err = json.Unmarshal(jsonBlob, &extractedToken)
	return extractedToken.Token, err
}

func GetUsers(token string) (string, error) {
	headers := make(map[string]string)
	headers["Authorization"] = "Bearer " + token
	content, err := call(Request{
		headers,
		"GET",
		strings.NewReader(""),
		url_base + "/user",
	})
	if err != nil {
		fmt.Println(err)
		return "", err
	}
	return content, err
}

func GetFTs(token string) (string, error) {
	headers := make(map[string]string)
	headers["Authorization"] = "Bearer " + token
	content, err := call(Request{
		headers,
		"GET",
		strings.NewReader(""),
		url_base + "/FT",
	})
	if err != nil {
		fmt.Println(err)
		return "", err
	}
	return content, err
}

func SetUsers(token string, UserDb string) ([]Volunteer, error) {
	var jsonUsers []Volunteer
	fmt.Println("Get userDB")
	if err := json.Unmarshal([]byte(UserDb), &jsonUsers); err != nil {
		fmt.Println(err)
	}
	return jsonUsers, nil
}

func SetSlots(token string, ftsDb string) ([]Slot, error) {
	var brutData DataStruct
	var FTs []FT
	_ = FTs
	var slots []Slot
	fmt.Println("Get SlotsDB")
	if err := json.Unmarshal([]byte(ftsDb), &brutData); err != nil {
		fmt.Println(err)
	}
	fmt.Println(brutData.Data)
	return slots, nil
}
