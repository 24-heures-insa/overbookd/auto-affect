package pkg

// Generated by https://quicktype.io
//
// To change quicktype's target language, run command:
//
//   "Set quicktype target language"

type Slot struct {
	FTid                  string
	Name                  string
	Instructions          string
	InCharge              string
	Location              string
	Status                string
	Date                  string
	Start                 string
	End                   string
	RequiredType          string
	RequiredTeam          string
	RequiredUser          string
	DriverLicenseRequired bool
}

func NewSlot(FTid, Name, Instructions, InCharge, Location, Status, Date, Start, End, RequiredType, RequiredTeam, RequiredUser string, DriverLicenseRequired bool) Slot {
	s := Slot{FTid, Name, Instructions, InCharge, Location, Status, Date, Start, End, RequiredType, RequiredTeam, RequiredUser, DriverLicenseRequired}
	return s
}

type Assignment struct {
	AssId     string
	Volunteer string
	Slot      string
}

func NewAssignment(AssId, Volunteer, Slot string) Assignment {
	a := Assignment{AssId, Volunteer, Slot}
	return a
}
