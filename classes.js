class Volunteer{
    constructor(firstname, lastname, nickname, team, friends, id, isCouple, sex, email, phone, handicap, hasDriverLicense, driverLicenseDate, birthdate, departement, year, availabilities) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.nickname = nickname;
        this.team = team;
        this.friends = friends;
        this.id = id;
        this.isCouple = isCouple;
        this.sex = sex;
        this.email = email;
        this.phone = phone;
        this.handicap = handicap;
        this.hasDriverLicense = hasDriverLicense;
        this.driverLicenseDate = driverLicenseDate;
        this.birthdate = birthdate;
        this.departement = departement;
        this.year = year;
        this.availabilities = availabilities;
    }
}

class Slot{
    constructor(FTid, name, instructions, inCharge, location, status, date, start, end, requiredType, requiredTeam, requiredUser, driverLicenseRequired) {
        this.FTid = FTid;
        this.name = name;
        this.instructions = instructions;
        this.inCharge = inCharge;
        this.location = location;
        this.status = status;
        this.date = date;
        this.start = start;
        this.end = end;
        this.requiredType = requiredType;
        this.requiredTeam = requiredTeam;
        this.requiredUser = requiredUser;
        this.driverLicenseRequired = driverLicenseRequired;
    }
}

class Assignment{
    constructor(timeslotId, volunteer, slot) {
        this.timeslotId = timeslotId;
        this.volunteer = volunteer;
        this.slot = slot;
    }
}

class AssignmentToPost{
    constructor(name, userId, by, FTid, timeslotId){
        this.name = name;
        this.userId = userId;
        this.by = by;
        this.FTid = FTid;
        this.timeslotId = timeslotId;
    }
}
module.exports.Volunteer = Volunteer;
module.exports.Slot = Slot;
module.exports.Assignment = Assignment;
module.exports.AssignmentToPost = AssignmentToPost;